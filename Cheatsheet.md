---
title: "Cheatsheet"
---
# Processes
## Process
A process is an abstraction of a running program and everything read or written
while it's running. A process can either be running, ready or blocked (waiting).
Depending on the OS there can be many other possible states. In OSes data
structures are used to track and manage processes.

## Direct Execution
With direct execution the operating system creates a process and runs the
`main()` function from that process. It gains controls again when the process
return from `main()`.

## Limited Direct Execution
For limited direct execution there's a distinction between user mode and kernel
mode. These are also sometimes referred to as CPU rings or domains.

Using so called system calls a process can return control back to kernel mode to
execute kernel actions. When doing this its context is saved by the CPU on the
kernel stack. A process uses a special trap CPU instruction (called an
interrupt) to invoke these system calls.

Using timer interrupts control is automatically returned to the OS at regular
intervals. This way the OS can switch between processes when needed. This is
called a context switch.

During the context switch the context of the current process is saved on the
kernel stack and then in kernel mode saved to the corresponding process
structure. The OS then decides which process gains control next.

## `fork`, `wait`, `exec`
The `fork()` syscall is a way the create new process on UNIX based systems. It
creates an almost exact copy of the calling process. It returns twice. In the
parent it returns the PID of the child or -1 (in case of failure) and the child
returns 0.

Using the `wait()` syscall enables the parent process to wait for child
termination.

To then start a different program the child process can use of the `exec()`
functions (which are a frontend to the `execve()` syscall). It replaces the
current process by loading a new program.

# Scheduling
## Policies
* **FIFO**: First In, First Out
* **SJF**: Shortest Job First
* **STCF**: Shortest Time-to-Completion First
* **Round Robin**: Run each job for a time slice.

## Multi-Level Feedback Queues (MLFQ)
With MLFQ the jobs are split in different queues which have an associated
priority. The jobs are then processed according to the following rules.

1. If $Prio(A)$ > $Prio(B)$: Run A
2. If $Prio(A)$ = $Prio(B)$: Run A and B in Round Robin
3. A new job is placed in the queue with the highest priority
4. When a job uses up all its assigned time at a given priority (regardless how
   often it has yielded the CPU), it is moved to the next lower priority.
5. After a given time period, move all jobs to the queue with the highest
   priority

![MLFQ](./assets/mlfq.png)

## Proportional Share Scheduling
Instead of optimizing for turnaround or response time this tries to guarantee a
certain amount of CPU time for each job. The easiest way is to use randomness.
Examples for this are lottery, and stride scheduling.

## Linux Scheduler
The completely fair scheduler (CFS) in Linux is a combination between MLFQ and
proportional share scheduling. Instead of traditional time slices it adjusts
them dynamically depending on the number of jobs.

Using so called **nice levels** one can modify job priorities. Depending on the
nice level (-20: highest, 19: lowest) a weight value is applied for calculating
the effective time slice of a job. Using the `nice` and `renice` utilities one
can modify these levels.

The two most important parameters for CFS are sched_latency (time before
considering a context switch) and sched_min_granularity (minimal length of a
time slice).

## Multiprocessor Scheduling
Scheduling on multiple CPUs (cores) can be quite difficult. One needs to
consider different possible problems. One of the biggest problems is the CPU
caches. It must be ensured that all caches maintain the same state regarding a
data item.

Otherwise you can both use a single queue for all CPUs or multiple queues e.g.
one per CPU. In practice both approaches can be found.

# Memory
Multiple processes and time sharing introduced challenges for memory management.
Because the entire memory needs to be exchanged at each process switch which is
highly inefficient. The solution to this is to have multiple processes
simultaneously in memory and this needs protection between the memory of the
processes.

## Address Space
The address space is the abstraction of physical as seen from a process. It
contains the full memory state, provides virtual adresses and provides
isolation.

![Address Space](./assets/address_space.png)

For a process the address space starts at address $0$ but in reality the address
space will be relocated and be at a different location in the physical memory. 

## Dynamic Relocation
A first approach was dynamic relocation. It introduces two hardware registers
called base and bounds. When loading a process the OS decides the size and start
position of the address space in physical memory. The base register is then set
the its start address and bounds to base + size. Memory references are then
translated at run time as follows:
$$
\text{physical address} = \text{base} + \text{virtual address}
$$

The upsides of dynamic relocation is that it can be implement in hardware using
only two registers and it offers the required protection. The drawbacks are that
it wastes memory because it suffers from internal fragmentation and makes it
hard to run programs with address spaces larger than physical memory.

![Internal Fragmentation](./assets/internal_fragmentation.png)

## Segmentation
Using individual base and bounds register per segment is called segmentation. A
segment is a single, contiguous region within an address space. Examples for
segments are `text`, `heap` and `stack`.

A segmentation table can look like the following:

![Segmentation Table](./assets/segmentation_table.png)

While segmentation reduces internal fragmentation, external fragmentation
becomes a key issue.

## Paging
The main idea is to use fixes-size pieces of address space and map the virtual
address space on those. These pieces are called pages and they divide the
physical memory into an array of fixed-size slots which are called page frames.

Paging requires a translation table between every virtual page of the address
space. This is a so called page table. Every process has its own page table. The
addrexss translation is quite simple. Split a virtual address into a virtual
page number (VPN) and a offset. Then you can translate the VPN into a physical
frame number (PFN) and use the same offset.

![Page Table](./assets/page_table_entry.png)

## Translation-Lookaside Buffers (TLBs)
Paging requires a lot of mapping information and generates an additional memory
lookup on every access. The solution is a cache of mapping in the MMU in the
hardware. On every memory access the MMU checks if the address is in the TLB. If
so it's a hit and address translation occurs without any page table access.
Otherwise it's a miss and the PTE must be retrieved from the page table and
stored in the TLB.

A TLB contains few (64 or 128) TLB entries and they work in both directions
(lookup using PFN and VPN possible). The valid bit indicates if an entry is a
valid translation.

![TLB](./assets/tlb.png)

As TLB entries are not bound to a specific process there's a problem when
performing a context switch. One solution is the simply flush the TLB on every
context switch the other solution is to mark the affiliated process in the TLB.

TLB fixes the performance issues of additional memory accesses but there's still
an issue of memory consumption as page table per process can take up quite a bit
of memory.

## Multi-level Page Tables
A multi-level page table is a way to get rid of all invalid regions in a page
table. The basic idea is to split the page table into page-sized chunks and if a
chunk contains no valid PTEs don't allocate it and use a page directory to look
up the page table chunks. A page directory is a simple table with one entry per
chunk of the page table. It contains page directory entries (PDE) which at least
contains a valid bit and the PFN of the page table chunk.

![Multi-level Page Tables](./assets/multi-level_page_table.png)

Multi-level page tables are more complex and on a TLB miss two (or more) memory
accesses are needed but the memory for the page table is allocated
proportionally to the space used by the process. It's also possible to use more
than two levels and it's required for larger address spaces.

## Swapping
Swap space is reserved disk space to swap out pages from memory. It is typically
implemented as a dedicated swap partition or as a swap file. The present bit in
the PTE is used to indicate if a page is in memory or has been swaped out. If
it's not set a page fault occurs. The page fault is handled by the OS as
accessing a disk is complicated.

The OS uses a so-called page-fault handler. Using the PTE the disk address of
the missing page can be determined and the handler then performs the necessary
disk I/O to laod the page and then updates the PTE's PFN. If no free space is
available in memory some other page must first be evicted.

![Swap Space](./assets/swap_space.png)

There are various replacement policies for deciding which page to swap out in
case no free pages are available.

* FIFO
* Random
* Least recently/frequently used (LRU/LFU)
* Clock

# Concurrency
## Threads
A thread is an abstraction for the current point of execution, allowing a
process to run concurrently at different code locations. Each thread of a
process shares the same virtual address space and open file descriptors. However
it is running with its own PC and registers. It also has its own stack and may
also have access to thread-local storage. Switching between threads requires a
context switch. Threads are used for parallel execution, when somethings is
blocking and for a background task.

![Threads](./assets/threads.png)

One can create threads using the POSIX threads (pthread) library. Important is
to not pass data from the stack to a thread, never return thread-local data from
a thread and always check the return codes of the pthread_* functions.

![pthread](./assets/pthread.png)

When multiple threads access the same variable a race condition can occur and
results are not deterministic. Such code is called a critical section. Using
syncronization primitives this can be circumvented.

## Locks
The basic idea of a lock is to ensure only a single thread is executing code in
a critical section at any time.

A mutual exclusion lock (mutex) is a variable which holds the state of the lock
(available, aquired).

![pthread Mutex](./assets/pthread_mutex.png)

There are multiple possibilities how a lock can be implemented. A simple
approach is to simply disable interrupts for a moment. This is bad as
controlling interrupts is a priveleged action and the OS looses control.

Another possibility is to use a simple flag variable to denote if a lock is
available or not. The problem is, it itself is prone to race conditions.

Using an HW instruction called test-and-set (atomic exchange, xchg) a lock can
be built. This instruction return sthe old value at a given location and sets it
to a new value atomically. Using this the flag variable lock can be transformed
to a valid spin lock. There are also some other instructions which can be used
for this like compare-and-swap, load-linked combined with store-conditional.

Using the fetch-and-add insturction a ticket-lock can be implemented which
compared to spin locks are fair. The problem of all the above locking mechanism
is the performance.

![Locks](./assets/locks.png)

The yield syscall moves the current thread from running to ready which allows
the OS to schedule another thread. Basically the thread de-schedules itself. The
problem with this is that it doesn't scale and it doesn't ensure fairness. That
means a queue is needed to track the order in which to wake up the threads.
Using the park and unpark syscalls this can be implemented but it may be prone
to a wake-up/waiting race.

A hybrid idea is to use a two-phase lock. First it tries spin-waiting and if
during that period the lock cannot be aquired it yields.

In Linux futex is used which is an arbitary 32-bit number in user memory. Which
supports a wait and wake operation.

## Condition Variables
Using a condition variables, threads can put themselves on a queue and wait
until a condition is true. Another thread can then wake on or more waiting
threads.

![pthread Conditon Variables](./assets/pthread_condition_variables.png)

## Semaphore
A semaphore is an object  with an integer value. It combines the functionality
of locks and condition variables. It is manipulated using two atomic functions:

* Incrementing: sem_post
  * Wakes up one thread if there is one or more waiting.
* Decrementing: sem_wait
  * Blocks the calling thread if the semaphore becomes negative.

You can use them as a lock. For that its initial value is set to 1. Such a
semaphore is called a binary semaphore:

![Binary Semaphore](./assets/binary_semaphore.png)

![Binary Semaphore with 2 Threads](./assets/binary_semaphore_2_threads.png)

Semaphores can also be used like a condition variable, when initialized to 0:

![Semaphore: Condition Variable](./assets/semaphore_conidtion.png)

## Reader-Writer Locks
A write lock is aquired when writing to the structure or by the first reading
thread. As long as the write lock is held no other thread can write to the
structure but can continue to read from the structure.

![pthread R-W Lock](./assets/pthread_rw_lock.png)

## Multi-Programming and IPC
Alternatives to writing multi-threaded applications are multi-programming and
inter-process communication (IPC: signals, shared memory, domain sockets).

## Memory-Mapped I/O
It is possible using mmap to map a part of a file into a process virtual memory.
It can then be accessed as any other memory location.

![mmap](./assets/mmap.png)

## Shared Memory
Using POSIX shared memory, two processes can share a memory region. This way
also mutexes, condition variables and semaphores can be exchanged. For
semaphores a convenient API through named semaphores is available.

![Shared Memory](./assets/shared_memory.png)

## Event-Based Concurrency
The main idea of events is simple. You have an event loop which waits for events
to occur and then you process the events accordingly. For this event handlers
are typically used.

# Persistence
## I/O Devices
I/O devices are typically attached to some sort of bus or interconnect. Two
parts are important in a canonical device the internal structure and the
hardware interface. For the OS only the hardware interface is important. The
hardware interface can be simplified to three kinds of registers: Status,
Command and Data. Interactions with a device can either happends through polling
or by using interrupts.

## Device Drivers
Device drivers provide an abstraction between different I/O devices and the OS.
Today they often represent the larger portion of the OS kernel code.

The driver talks to the device using its specific device interface and then
passes the information on to a more generic interface of the OS. Common generic
interfaces are block and character devices.

![File System Stack](./assets/file_system_stack.png)

## Hard Disk
HDDs consist of a large number of 512-byte blocks (sectors) which can be
individually adressed. They consist of multiple rotating disks (platters) fixed
on a spindle rotating fast. On every side blocks are encoded in concentric
tracks. A disk head is attached on an arm which move across the tracks.

Compared to RAM, HDDs are far slower. This is mostly due to various delays like
seek time, rotational delay and transfer time.

![Time and Rate](./assets/time_and_rate.png)

Modern HDDs contain a local cache which holds data read from or to be written to
the disk. Which allows some flexibility e.g. the on-disk controller rcan read
more than requested or can optimize writing to blocks located on different
tracks.

An OS typically performas some sort of disk scheduling. When the properties are
known the length of a job can be calculated and the SJF (shorted job first)
principle is possible. As modern HDDs do not expose their internals to the OS
there is less the OS can do. Only a small amount of disk requests is scheduled
by the OS and the final scheduling then occurs in the disk. Following are some
possible principles for scheduling:

* Shortest Seek Time First (SSTF)
* Elevator Algorithms (SCAN, C-SCAN, F-SCAN)
* Shortest Positioning First (SPTF)

## RAID (Redundant Array of Independent Disks)
RAID systems can improve one or more of the following: performance, capacity or
reliability. A RAID system consists of several HDDs which look like a single HDD
for the host computer. Implementation can be done in hard- or software.

* RAID Level 0 (Striping)
  * Not really a RAID level as there is no redundancy.
  * It stripes blocks across all disks
* RAID Level 1 (Mirroring)
  * Provides redundancy of $N - 1$ disks
* RAID 10
  * Combines two or more mirror pairs into a RAID 0
* RAID 01
  * Mirror of striped pairs.
  * The failure of a single disks always leads to the loss of a complete stripe
* RAID Level 4 (Parity)
  * An additional disk is storing paritiy information
* RAID Level 5 (Rotating Parity)
  * Store paritiy across all disks
* RAID Level 6 (Double Parity)
  * Have redundant parity information

## Files and Directories
Files and directories can be understood as a basic abstraction of persistent storage. A
file is a linear array of bytes which can be read and written. Each file has an inode number
and each file has some additional metadata. The OS has no concept of the structure of a file.

A directory is a special type of file. It also has an inode number but its contents is a 
list of (inode, name) pairs, which map internal low-level names to user-readable names.

On UNIX systems not only real files can be found but also devices, named pipes and virtual
file systems which are managed using the same file API.

![POSIX File API](./assets/file_api.png)

![Directory API](./assets/directory_api.png)

Each process has initially three open file descriptors:

* 0: stdin: Reading input
* 1: stdout: Writing output
* 2: stderr: Seperate channel for error output

Every process keeps track of its open file descriptors.

Hard links are an additional name that is linked to an existing inode and symbolic links
is kind of file which is a sort of pointer to another file.

![File Overview](./assets/file_overview.png)

As files and directories are shared between users and processes an additional access control
mechanism is required. On Linux permission bits are used. Three different permissions (read, write, and execute) can apply to either the user, its group or to all others. Using the
`chmod`, `chown` and `chgrp` commands these permissions can be adjusted.

## File System Structures
A FS divides a disk into blocks which is a fixed size unit. The blocks are divided into
two parts: data region and inode tables. For each a fixed amount of blocks is allocated
when the FS is created.

The FS needs to track which blocks are free or in use. For this some sort of allocation structure like a free list or a bitmap is required. A FS needs to store additional metadata
about a FS instance in a superblock. It contains a magic number (identifying the FS type) and the amount of inode- and data blocks.

## Inode
An inode is a generic term used for the data structure holding the metadata of a given file.
Its contents are FS dependent and typically contain things like the file-size, data-block pointers, owner, permissions and dates. The name stands for index node.

![Multi-Level Index](./assets/multi-level_index.png)

## Access Methods
### Reading a File
1. The inode of “bar” needs to be found by directory traversal:
     1. Locate the inode of “/” (known to the FS, often 2)
     2. It is a directory, read its data blocks
     3. Locate the inode of “foo”. If it is also a directory, go back to 1.2
2. Read the inode of “bar”, check permissions, . . . , allocate and
return a file descriptor
3. Read the 3 data blocks:
   1. Read the inode to find the address of the block
   2. Read the data block
   3. Update the inode of “bar” (e.g. last-accessed time)

## Creating a File
1. First, the file needs to be created:
   1. The inode of “foo” needs to be found, requiring a directory
   traversal as described on Slide 19
   2. A new inode needs to be allocated for “bar”, requiring a read
   and a write of the inode bitmap
   3. The new inode of “bar” needs to be written
   4. The data of “foo” needs to be updated with name and inode
   number of “bar”
   5. The inode of “foo” needs to be updated (e.g. last-accessed
   time)
2. The 3 data blocks can now be written (5 I/O requests each):
   1. A free block must be found, requiring a read and write of the
   data bitmap
   2. A read and a write are required to update the inode of “bar”
   3. Finally, the data block is written

## Crash Consistency
From crashes a number of issues can arrise in the FS like data loss, file system incosistency and space leaks. An early solution for this problem were file system checkers.

The file system checker performs some checks on the FS like sanity checks of the superblock and inodes and restores allocation structures based on the inode contents. File system checking nowadays is impractical as it's a very slow process.

## Journaling
Another approch for crash consistency is journaling or write-ahead logging. The basic idea
is that before performing the effective updates a log of what will be done is written. Then
if a crash occurs during the updates the log can be consulted and the updates can be replayed. Normal writes will take a bit more time but crash recovery is much faster.

Journaling adds an additional FS structure: the journal. It is placed somewhere on the disk.

![Journaling](./assets/journaling.png)

## Log-Structued File Systems (LFS)
To avoid writing data twice, the LFS only write the log to disk and used its structure for
the FS contents. The idea is to perform all writes sequentially. Also writes should either
be contiguous or large.

LFS also uses journaling for crash consistency.